variable "resource_group_name" {
  description = "The name of the resource group in which to create the AKS cluster."
  default = "polyconseil-challenge"
}

variable "resource_group_location" {
  description = "The Azure region in which to create the AKS cluster."
  default = "eastus"
}

variable "aks_cluster_name" {
  description = "The name of the AKS cluster."
  default = "helpet-aks-cluster"
}


variable "dns_prefix" {
  description = "DNS prefix for the AKS cluster."
  default = "myakscluster"
}

variable "node_count" {
  description = "Number of nodes in the AKS cluster."
  default     = 1
}

variable "vm_size" {
  description = "The size of the VMs in the AKS cluster."
  default     = "Standard_B2s"
}

variable "client_secret" {
  description = "The client secret for the Service Principal."
}