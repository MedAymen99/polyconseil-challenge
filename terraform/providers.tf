terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "3.90.0"
    }

  }
}


provider "azurerm" {
  features {}

  subscription_id = "bcfd15fd-cfda-4dab-b575-b826ed03175d"
  tenant_id       = "dbd6664d-4eb9-46eb-99d8-5c43ba153c61"
  client_id       = "0a2062b1-2074-4a6c-a7f4-b65d6a4224aa"
  client_secret   = var.client_secret

}
