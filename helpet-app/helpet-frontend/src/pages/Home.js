import React from "react"
import Intro from "../Components/Intro"
import SearchBar from "../Components/SearchBar"
import HomePetCards from "../Components/HomePetCards"
import PaginationButtons from "../Components/PaginationButtons"

export default function Home(props) {

  return (
    <div className="page-content">
      <Intro />
      <SearchBar />
      <HomePetCards />
      <div className="pagination">
        <PaginationButtons />
      </div>
    </div>
  )
}
