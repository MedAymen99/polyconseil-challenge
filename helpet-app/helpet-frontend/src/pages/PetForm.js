import React, { useContext, useState } from "react"
import { NavigationContext } from "../context/NavigationContext";
import useApi from '../utils/api';
import { PaginationContext } from '../context/PaginationContext';
import { handleChangeFunc } from "../utils/generalFunctions";
import { useDispatch, useSelector } from "react-redux";
import { updatePetList, setUpdatePetData} from "../redux/petSlice";


export default function PetForm(props) {
    
    // Redux state
    const dispatch = useDispatch();
    const selectedPetId = useSelector(state => state.pet.selectedPetId);
    const updatePetData = useSelector(state => state.pet.updatePetData);
    
    // Local state
    const [addPetData, setAddPetData] = useState({});
    const [selectedImageFile, setSelectedImageFile] = useState(null);
    
    // Context (keeping these for now)
    const navigate = useContext(NavigationContext);
    const { setCurrentPage, numberOfPages } = useContext(PaginationContext);
    
    const api = useApi();
    const formData = props.update ? updatePetData : addPetData;
    const setFormData = props.update ? 
        (data) => dispatch(setUpdatePetData(data)) : 
        setAddPetData;

    const handleChangeImage = (event) => {
        const imageFile = event.target.files[0];
        console.log(event.target.files[0])
        setSelectedImageFile(imageFile);
    }

    const handleChange = handleChangeFunc(setFormData);
    
    const petFormData = (petData) => {
        const data = new FormData();
        data.append("name", petData.name);
        data.append("type", petData.type);
        data.append("breed", petData.breed);
        data.append("age", petData.age);
        data.append("sex", petData.sex);
        data.append("description", petData.description);
        if (selectedImageFile) {
            data.append("imageFile", selectedImageFile);
        }
        return data;
    }

    const addPet = async () => {
        const data = petFormData(addPetData);
        console.log()
        try {
        const response = await api.post("/api/pets/create", data);
        console.log("response")
        setCurrentPage(numberOfPages)
        } catch(err) {
          alert(err);
        }
        navigate('/')
    };

    const updatePet = async () => {
        const data = petFormData(updatePetData);
        try {
            const response = await api.put(`/api/pets/update/${selectedPetId}`, data) ;
            const payload = {
                selectedPetId,
                updatePetData 
            }
            dispatch(updatePetList(payload))
        } catch (err) {
            console.log(err)
        }
        
        //event.preventDefault()
        //Router.History.back();
        navigate('/')
        
    };

    return (
        <div className="PetForm">
            <div className="form-container">
                <form onSubmit={props.update ? updatePet : addPet} className="pet-form animate form" >
                <h1>{props.update ? "Update Your Post" : "Give up for adoption"}</h1>
                    <div className="fields">
                        <div>
                            <div className="form-field">
                                <label htmlFor="name">name</label>
                                <input
                                    type="text"
                                    placeholder="name"
                                    onChange={handleChange}
                                    name="name"
                                    value={formData.name}
                                    required
                                />
                                <br />
                            </div>   
                            <div className="form-field">
                                  <label htmlFor="type" >type</label>
                                  <select
                                    name="type"
                                    id="type"
                                    onChange={handleChange}
                                    value={formData.type}
                                    required
                                  >
                                    <option selected value=""> Type </option>
                                    <option value="dog" /*defaultValue*/>dog</option>
                                    <option value="cat">cat</option>
                                    <option value="rabbit">rabbit</option>
                                    <option value="mouse">mouse</option>
                                    <option value="other">other</option>
                                  </select>
                                <br />
                            </div>
                            <div className="form-field">
                                <fieldset>
                                    <legend>What is their sex?</legend>
                                    <input
                                        type="radio"
                                        id="male"
                                        name="sex"
                                        value="male"
                                        checked={formData.sex === "male"}
                                        onChange={handleChange}
                                    />
                                    <label htmlFor="male"> male</label>
                                    <br />
                                    <input
                                        type="radio"
                                        id="female"
                                        name="sex"
                                        value="female"
                                        checked={formData.sex === "female"}
                                        onChange={handleChange}
                                    />
                                    <label htmlFor="female"> female</label>
                                </fieldset>
                                <br />
                            </div>
                            <div className="form-field">
                              <label htmlFor="description">description</label>
                              <textarea
                                  type="text"
                                  placeholder="description"
                                  onChange={handleChange}
                                  name="description"
                                  value={formData.description}
                                  maxLength={500}
                              />
                              <br />
                            </div>
                        </div>
                        <div>
                            <div className="form-field">
                                <label htmlFor="age">age (in months)</label>
                                <input
                                    type="number"
                                    placeholder="months"
                                    onChange={handleChange}
                                    name="age"
                                    value={formData.age}
                                    required
                                />
                                <br />
                            </div>
                            <div className="form-field">
                                <label htmlFor="breed">breed</label>
                                <input
                                    type="text"
                                    placeholder="breed"
                                    onChange={handleChange}
                                    name="breed"
                                    value={formData.breed}
                                    required
                                />
                                <br />
                            </div>
                            <div className="form-field">
                                <label htmlFor="image">upload image</label>
                                <input
                                    type="file"
                                    onChange={handleChangeImage}
                                    name="image"
                                    // accept=".jpg"
                                />
                                <br />
                            </div>
                        </div>            
                    </div>
                    <button className="form-button">
                    {props.update ? "Update pet" : "Add pet"}
                    </button>
                </form>
            </div>
        </div>
        
    )
}
