import React, { useContext } from "react"
import { AuthContext } from './../utils/auth/AuthContext';
import MyProfilePetCards from "../Components/MyProfilePetCards";

export default function MyProfile(props) {
    const {auth} = useContext(AuthContext)
  
    const user = auth.user || JSON.parse(localStorage.getItem('user'))
    
    return(
    <div className="page-content">
        <h1 className="welcome-user">Welcome {user.username}</h1>
        <h2>These are your adoption posts :</h2>
        <MyProfilePetCards />
    </div>
    )

}