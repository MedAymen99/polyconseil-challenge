import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  petList: [],
  isLoading: true,
  selectedPetId: null,
  updatePetData: {},
  searchData: {
    type: "",
    age: "",
    sex: ""
  }
};

const petSlice = createSlice({
    name:'pet',
    initialState,
    reducers: {
        setPetList: (state, action) => {
          state.petList = action.payload
        },
        updatePetList : (state, action) => {
          const {selectedPetId, updatePetData } = action.payload;
          const selectedPetIndex = state.petList.findIndex(pet => pet.id == selectedPetId)
          const item = {
              id : selectedPetId,
              ...updatePetData
          }
          state.petList.splice(selectedPetIndex,1,item);
        },
        setSelectedPetId: (state, action) => {
          state.selectedPetId = action.payload;
        },
        setUpdatePetData: (state, action) => {
          state.updatePetData = action.payload;
        },
        selectPetForUpdate: (state, action) => {
          const petItem = action.payload;
          state.selectedPetId = petItem.id;
          const { id, ...data } = petItem;
          state.updatePetData = data;
        },
        //reducers for search state
        setSearchData: (state, action) => {
          state.searchData = action.payload;
        },
        updateSearchField: (state, action) => {
          const { name, value, checked, type } = action.payload;
          state.searchData[name] = type === "checkbox" ? checked : value;
        },      
        toggleSearchTrigger: (state) => {
          state.searchTrigger = !state.searchTrigger;
        },

    },
})

export const { 
  setPetList, 
  updatePetList, 
  setSelectedPetId, 
  setUpdatePetData,
  selectPetForUpdate,
  setSearchData,
  updateSearchField,
  toggleSearchTrigger,
} = petSlice.actions;


export default petSlice.reducer;
