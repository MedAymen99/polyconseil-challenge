import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import App from './App';
import './style.css';
import AuthProvider from './utils/auth/AuthContext';
import { NavigationProvider } from './context/NavigationContext';
import { PaginationProvider } from './context/PaginationContext';
import { store } from './redux/store';
import { BrowserRouter } from 'react-router-dom';

const root = ReactDOM.createRoot(document.getElementById('root'));

root.render(
  // <React.StrictMode>  
    <Provider store={store}>
      <BrowserRouter>
        <AuthProvider>
            <NavigationProvider>
              <PaginationProvider>
                <App />
              </PaginationProvider>
            </NavigationProvider>
        </AuthProvider>
      </BrowserRouter>
    </Provider>
  // </React.StrictMode>
);