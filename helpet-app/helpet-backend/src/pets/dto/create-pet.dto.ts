import { IsIn, IsOptional, Max, MaxLength, ValidationArguments,  } from "class-validator";

export class CreatePetDto {
    name: string;

    type: string;

    breed: string;
    
    @IsIn(["male", "female"])
    sex: string;

    @Max(600,{
        message: (validationData: ValidationArguments) => {
            return `la taille minimale de ${validationData.property} est ${validationData.constraints[0]}`
        }
    })
    age: number;

    @IsOptional()
    description: string;

    imageRef: string;
}